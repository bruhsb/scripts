#!/bin/bash
source '/opt/files/tools/scripts/install_tools.sh'

sudo dnf update -y

sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-`rpm -E %fedora`.noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-`rpm -E %fedora`.noarch.rpm

sudo dnf -y copr enable dawid/better_fonts &&\
	sudo dnf -y install fontconfig-enhanced-defaults fontconfig-font-replacements

sudo dnf -y copr enable kwizart/fedy && sudo dnf -y install fedy

sudo cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf_bkp && \
	sudo echo "fastestmirror=true" >> /etc/dnf/dnf.conf && \
	sudo echo "deltarpm=true" >> /etc/dnf/dnf.conf

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc && \
cat <<EOF | sudo tee /etc/yum.repos.d/vscode.repo
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF

sudo dnf -y copr enable jdoss/slack-repo
sudo dnf -y install slack-repo
sudo dnf -y install slack


sudo dnf -y install \
	htop nmap telnet glances dropbox exa git zsh gparted testdisk plank tilix \
	gimp code gpaste arc-theme papirus-icon-theme \
	gcc kernel-headers kernel-devel akmod-nvidia xorg-x11-drv-nvidia xorg-x11-drv-nvidia-libs xorg-x11-drv-nvidia-libs.i686 \
	mozilla-fira-mono-fonts \
	mozilla-fira-sans-fonts \
	mozilla-fira-fonts-common \
	fira-code-fonts openssh-askpass

wget https://cinnamon-spices.linuxmint.com/files/themes/CBlack-Remix.zip -O /tmp/CBlack-Remix.zip && \
unzip /tmp/CBlack-Remix.zip -d /home/$USER/.themes

sudo rpm --import https://raw.githubusercontent.com/UnitedRPMs/unitedrpms/master/URPMS-GPG-PUBLICKEY-Fedora && \
	sudo dnf -y install https://github.com/UnitedRPMs/unitedrpms/releases/download/15/unitedrpms-$(rpm -E %fedora)-15.fc$(rpm -E %fedora).noarch.rpm && \
	sudo dnf -y install gstreamer1-{libav,plugins-{good,ugly,bad{-free,-nonfree}}} --setopt=strict=0 && \
	sudo dnf -y install vlc mpv ffmpeg celluloid

sudo dnf update -y

sudo dnf -y install libatomic && \
sudo ln -s /opt/files/configs/tools/popcorntime/Popcorn-Time /usr/local/bin/Popcorn-Time && \
sudo tee -a /usr/share/applications/popcorntime.desktop << EOM
[Desktop Entry]
Name=Popcorn-Time
Exec=/usr/local/bin/Popcorn-Time
Icon=popcorntime
Type=Application
Categories=AudioVideo;Player;
Keywords=popcorn;
EOM

## Creating Folders && Links

folders=`ls /opt/files/configs/home/`

for folder in $folders; do
  ln -s "/opt/files/configs/home/$folder" "/home/$USER/.$folder"
done

folders=`ls /opt/files/configs/gnome-config/`

for folder in $folders; do
  ln -s "/opt/files/configs/gnome-config/$folder" "/home/$USER/.config/$folder"
done

folders=`ls /opt/files/home_folders/`

for folder in $folders; do
  rm -fr "/home/$USER/$folder" && ln -s "/opt/files/home_folders/$folder" "/home/$USER/$folder"
done

## Install Work Tools

TERRAFORM_VERSION=0.12.25

terraform_install $TERRAFORM_VERSION
gcloud_install

eval "$(ssh-agent -s)"
git config --global user.email "bruh-sb@live.com" && git config --global user.name "bruhsb"

sudo chsh -s $(which zsh) $USER
dconf load /com/gexperts/Tilix/ < '/opt/files/bkps/bkp_tilix_settings.dconf'
dconf load /org/cinnamon/ < '/opt/files/bkps/bkp_cinnamon_settings.dconf'


