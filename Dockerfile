FROM fedora

RUN dnf install --setopt=tsflags=nodocs git npm -y && dnf clean all && npm install -g @hutson/semantic-delivery-gitlab