#!/bin/bash

terraform_install(){

    TERRAFORM_VERSION=$1

    echo "Installing Terraform ${TERRAFORM_VERSION}"

    wget -q -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
        && sudo unzip /tmp/terraform.zip -d /usr/local/bin \
        && rm -rf /tmp/terraform.zip

}

gcloud_install(){
    sudo tee -a /etc/yum.repos.d/google-cloud-sdk.repo << EOM
[google-cloud-sdk]
name=Google Cloud SDK
baseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOM

    sudo dnf install google-cloud-sdk -y && gcloud init && gcloud components install kubectl

}

krew_install(){
(
  set -x; cd "$(mktemp -d)" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
  tar zxvf krew.tar.gz &&
  KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
  "$KREW" install --manifest=krew.yaml --archive=krew.tar.gz &&
  "$KREW" update
)
}

kubectl_install(){

    sudo dnf -y install kubectl
    krew_install
    sudo ln -s /opt/files/tools/kubectx/kubectx /usr/local/bin/kubectx
    sudo ln -s /opt/files/tools/kubectx/kubens /usr/local/bin/kubens

}

azure_install(){

    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo sh -c 'echo -e "[azure-cli]
name=Azure CLI
baseurl=https://packages.microsoft.com/yumrepos/azure-cli
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/azure-cli.repo'

    sudo dnf -y install azure-cli

}
