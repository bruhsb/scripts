#!/bin/bash

TERRAFORM_VERSION=$1

echo "Installing or Upgrade Terraform ${TERRAFORM_VERSION}"

wget -q -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && sudo unzip /tmp/terraform.zip -d /usr/local/bin \
    && rm -rf /tmp/terraform.zip