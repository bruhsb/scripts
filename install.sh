#!/bin/bash

echo -e "Creating Symbolic Links *sudo required!\n"

sudo ln -s /opt/scripts/upgrade-terraform.sh /usr/local/bin/upgrade-terraform && \
    echo -n "Upgrade Terraform Tool: Done!"

echo -e "Install tf-lint\n"

./install-tflint.sh